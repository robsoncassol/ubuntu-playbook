#!/bin/bash
# rm -rf roles/galaxy
ansible-galaxy install -p roles/galaxy/ -r roles/requirements.yml
ansible-playbook -i assets/hosts site.yml -e username=$USER

sudo ln -sf /home/docker /var/lib/docker

echo 'Ansible install finished!'
