#!/bin/bash
USER=runner docker run -it --rm -v `pwd`:/runner --name ubuntu-playbook-test fabiohbarbosa/ansible-runner:ubuntu-16.10-yakkety bash setup.sh
USER=runner docker run -it --rm -v `pwd`:/runner --name ubuntu-playbook-test fabiohbarbosa/ansible-runner:ubuntu-17.04-zesty bash setup.sh